#sudo easy_install requests
import json
import requests
import sys
import pickle
import os
LEVEL   =   2
target  =   8121948
me      =   8951720
PATH    =   "./mainmap.p"
MAINMAP = {}
rows, columns = os.popen('stty size', 'r').read().split()
if os.path.isfile(PATH) and os.access(PATH, os.R_OK):
    MAINMAP = pickle.load( open( PATH , "rb" ) )

def prnt(data):
    sys.stdout.write('%s\r' % ' '*columns)
    sys.stdout.write('%s\r' % data)
    sys.stdout.flush()

def getFriends(id):
    if id in MAINMAP:
        return MAINMAP[id]['friends']
    elif id == -1:
        return []
    else:
        MAINMAP[id] = {}
    try:{
        response = requests.get("https://api.vk.com/method/friends.get?user_id="+str(id))
        respJson = json.loads(response.text)
        if 'response' in respJson:
            MAINMAP[id]['friends'] = respJson['response']
            return respJson['response']
        elif 'error' in respJson :
            #print "\nfriends closed for {0}".format(id)
            MAINMAP[id]['friends'] = []
        else:
            print "\nproblem with id {0}".format(id)
            print respJson
  except:
        return [-1]
    return []

def findIntersection(level, parent, array, searchKey):
    #x = ">processing friends for {0} ({1})".format(parent, len(array))
    #print ""
    #print x.rjust(len(x)+level*2, '>')
    it = 0
    for friend in array:
        it = it + 1
        prnt(" "*100)
        x = ">processing friend {0} ({1} of {2} for {3})".format(friend, it, len(array), parent)
        x = x.rjust(len(x)+level*2, '>')
        prnt(x)

        if friend == searchKey:
            yield (parent,)
            break            
        else:
            if LEVEL > level:
                for i in findIntersection(level+1, friend, getFriends(friend), searchKey):
                    yield (parent,) + i
result = []
for r in findIntersection(0, target, getFriends(target), me):
    result.append(r)

print "."*10
print "Searched connections between you ({0}) and target({1}) ".format(me, target)
print "found {0} connections ".format(len(result))
print "="*10
for r in result:
    print "connection length = " + str(len(r))
    for t in r:
        print "\thttp://vk.com/id{0}".format(t)
    print "-"*10

pickle.dump(MAINMAP, open( PATH, "wb" ))




